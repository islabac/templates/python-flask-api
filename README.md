# python-flask-api

This is a template API using Flask. 

### How to use?

1. Clone the repository with `$ git clone _REPO_ADDRESS_`.
2. Create a new `.env` file inside `app` folder with `$ cp app/.env-sample app/.env`.
3. Run the application with `$ flask run` or `$ python main.py`.
4. Rename the root folder to your from `python-flask-api` to `your-projectname-api`.

### Push the application

To push the application to a different repository, user can re-initialize the repository
by following the steps.

1. Remove the existing git with `$ rm -rf .git`.
2. Re-initialize git with `$ git init`.
3. Add a remote with `$ git remote add origin _REPO_ADDRESS_`.

### Deploy the application

Use can deploy the application using `PM2` with `$ pm2 start ecosystem.config.json`. Make necessary 
changes to `ecosystem.config.json` file with correct `interpreter`, `log_file`, `error_file`, etc.
"""
Template API blueprint routes
Author: Avanish Shrestha, 2019
Organization: Intelligent Systems Laboratory
"""

from __future__ import division, print_function, absolute_import

# pylint: disable=W0401
from . import example
from .example import *
# pylint: enable=W0401

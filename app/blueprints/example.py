"""
Template API Example Routes
Author: Avanish Shrestha, 2019
Organization: Intelligent Systems Laboratory
"""
from __future__ import division, print_function, absolute_import

import traceback
from flask import jsonify, request, Blueprint

from app.util import verify_headers

example = Blueprint('example', __name__)

@example.route('/', methods=['GET'])
def main():
    """ API endpoint for example route
    """
    try:
        message = {
            'data': {
                'message': 'This is an example route'
            }
        }
        response = jsonify(message)
        response.status_code = 200
        return response
    except Exception as inst:
        print(traceback.print_exc())
        message = {
            'error' : {
                'code' : 404,
                'message' : str(inst)
            }
        }
        response = jsonify(message)
        response.status_code = 404
        return response

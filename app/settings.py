"""
Settings for python dotenv
Author: Avanish Shrestha, 2019
Organization: Intelligent Systems Laboratory
"""

import os
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
dotenv_path = os.path.join(basedir, '.env')
load_dotenv(dotenv_path)

HOST = os.environ.get("HOST")
PORT = int(os.environ.get("PORT"))
# pylint: disable=W0123
# pylint: enable=W0123

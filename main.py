"""
Template Flask API
Author: Avanish Shrestha, 2019
Organization: Intelligent Systems Laboratory
"""

from app import app
import app.settings as env

if __name__ == '__main__':
    app.run(host=env.HOST, port=env.PORT)
